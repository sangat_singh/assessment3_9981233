﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------



namespace UWPContactApp
{
    public partial class App : global::Windows.UI.Xaml.Markup.IXamlMetadataProvider
    {
    private global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlTypeInfoProvider _provider;

        /// <summary>
        /// GetXamlType(Type)
        /// </summary>
        public global::Windows.UI.Xaml.Markup.IXamlType GetXamlType(global::System.Type type)
        {
            if(_provider == null)
            {
                _provider = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlTypeInfoProvider();
            }
            return _provider.GetXamlTypeByType(type);
        }

        /// <summary>
        /// GetXamlType(String)
        /// </summary>
        public global::Windows.UI.Xaml.Markup.IXamlType GetXamlType(string fullName)
        {
            if(_provider == null)
            {
                _provider = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlTypeInfoProvider();
            }
            return _provider.GetXamlTypeByName(fullName);
        }

        /// <summary>
        /// GetXmlnsDefinitions()
        /// </summary>
        public global::Windows.UI.Xaml.Markup.XmlnsDefinition[] GetXmlnsDefinitions()
        {
            return new global::Windows.UI.Xaml.Markup.XmlnsDefinition[0];
        }
    }
}

namespace UWPContactApp.UWPContactApp_XamlTypeInfo
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal partial class XamlTypeInfoProvider
    {
        public global::Windows.UI.Xaml.Markup.IXamlType GetXamlTypeByType(global::System.Type type)
        {
            global::Windows.UI.Xaml.Markup.IXamlType xamlType;
            if (_xamlTypeCacheByType.TryGetValue(type, out xamlType))
            {
                return xamlType;
            }
            int typeIndex = LookupTypeIndexByType(type);
            if(typeIndex != -1)
            {
                xamlType = CreateXamlType(typeIndex);
            }
            if (xamlType != null)
            {
                _xamlTypeCacheByName.Add(xamlType.FullName, xamlType);
                _xamlTypeCacheByType.Add(xamlType.UnderlyingType, xamlType);
            }
            return xamlType;
        }

        public global::Windows.UI.Xaml.Markup.IXamlType GetXamlTypeByName(string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                return null;
            }
            global::Windows.UI.Xaml.Markup.IXamlType xamlType;
            if (_xamlTypeCacheByName.TryGetValue(typeName, out xamlType))
            {
                return xamlType;
            }
            int typeIndex = LookupTypeIndexByName(typeName);
            if(typeIndex != -1)
            {
                xamlType = CreateXamlType(typeIndex);
            }
            if (xamlType != null)
            {
                _xamlTypeCacheByName.Add(xamlType.FullName, xamlType);
                _xamlTypeCacheByType.Add(xamlType.UnderlyingType, xamlType);
            }
            return xamlType;
        }

        public global::Windows.UI.Xaml.Markup.IXamlMember GetMemberByLongName(string longMemberName)
        {
            if (string.IsNullOrEmpty(longMemberName))
            {
                return null;
            }
            global::Windows.UI.Xaml.Markup.IXamlMember xamlMember;
            if (_xamlMembers.TryGetValue(longMemberName, out xamlMember))
            {
                return xamlMember;
            }
            xamlMember = CreateXamlMember(longMemberName);
            if (xamlMember != null)
            {
                _xamlMembers.Add(longMemberName, xamlMember);
            }
            return xamlMember;
        }

        global::System.Collections.Generic.Dictionary<string, global::Windows.UI.Xaml.Markup.IXamlType>
                _xamlTypeCacheByName = new global::System.Collections.Generic.Dictionary<string, global::Windows.UI.Xaml.Markup.IXamlType>();

        global::System.Collections.Generic.Dictionary<global::System.Type, global::Windows.UI.Xaml.Markup.IXamlType>
                _xamlTypeCacheByType = new global::System.Collections.Generic.Dictionary<global::System.Type, global::Windows.UI.Xaml.Markup.IXamlType>();

        global::System.Collections.Generic.Dictionary<string, global::Windows.UI.Xaml.Markup.IXamlMember>
                _xamlMembers = new global::System.Collections.Generic.Dictionary<string, global::Windows.UI.Xaml.Markup.IXamlMember>();

        string[] _typeNameTable = null;
        global::System.Type[] _typeTable = null;

        private void InitTypeTables()
        {
            _typeNameTable = new string[13];
            _typeNameTable[0] = "UWPContactApp.DateFormatConverter";
            _typeNameTable[1] = "Object";
            _typeNameTable[2] = "UWPContactApp.IntegerToStringConverter";
            _typeNameTable[3] = "UWPContactApp.MainPage";
            _typeNameTable[4] = "Windows.UI.Xaml.Controls.Page";
            _typeNameTable[5] = "Windows.UI.Xaml.Controls.UserControl";
            _typeNameTable[6] = "System.Collections.ObjectModel.ObservableCollection`1<UWPContactApp.Contact>";
            _typeNameTable[7] = "System.Collections.ObjectModel.Collection`1<UWPContactApp.Contact>";
            _typeNameTable[8] = "UWPContactApp.Contact";
            _typeNameTable[9] = "Int32";
            _typeNameTable[10] = "String";
            _typeNameTable[11] = "System.DateTime";
            _typeNameTable[12] = "System.ValueType";

            _typeTable = new global::System.Type[13];
            _typeTable[0] = typeof(global::UWPContactApp.DateFormatConverter);
            _typeTable[1] = typeof(global::System.Object);
            _typeTable[2] = typeof(global::UWPContactApp.IntegerToStringConverter);
            _typeTable[3] = typeof(global::UWPContactApp.MainPage);
            _typeTable[4] = typeof(global::Windows.UI.Xaml.Controls.Page);
            _typeTable[5] = typeof(global::Windows.UI.Xaml.Controls.UserControl);
            _typeTable[6] = typeof(global::System.Collections.ObjectModel.ObservableCollection<global::UWPContactApp.Contact>);
            _typeTable[7] = typeof(global::System.Collections.ObjectModel.Collection<global::UWPContactApp.Contact>);
            _typeTable[8] = typeof(global::UWPContactApp.Contact);
            _typeTable[9] = typeof(global::System.Int32);
            _typeTable[10] = typeof(global::System.String);
            _typeTable[11] = typeof(global::System.DateTime);
            _typeTable[12] = typeof(global::System.ValueType);
        }

        private int LookupTypeIndexByName(string typeName)
        {
            if (_typeNameTable == null)
            {
                InitTypeTables();
            }
            for (int i=0; i<_typeNameTable.Length; i++)
            {
                if(0 == string.CompareOrdinal(_typeNameTable[i], typeName))
                {
                    return i;
                }
            }
            return -1;
        }

        private int LookupTypeIndexByType(global::System.Type type)
        {
            if (_typeTable == null)
            {
                InitTypeTables();
            }
            for(int i=0; i<_typeTable.Length; i++)
            {
                if(type == _typeTable[i])
                {
                    return i;
                }
            }
            return -1;
        }

        private object Activate_0_DateFormatConverter() { return new global::UWPContactApp.DateFormatConverter(); }
        private object Activate_2_IntegerToStringConverter() { return new global::UWPContactApp.IntegerToStringConverter(); }
        private object Activate_3_MainPage() { return new global::UWPContactApp.MainPage(); }
        private object Activate_6_ObservableCollection() { return new global::System.Collections.ObjectModel.ObservableCollection<global::UWPContactApp.Contact>(); }
        private object Activate_7_Collection() { return new global::System.Collections.ObjectModel.Collection<global::UWPContactApp.Contact>(); }
        private object Activate_8_Contact() { return new global::UWPContactApp.Contact(); }
        private void VectorAdd_6_ObservableCollection(object instance, object item)
        {
            var collection = (global::System.Collections.Generic.ICollection<global::UWPContactApp.Contact>)instance;
            var newItem = (global::UWPContactApp.Contact)item;
            collection.Add(newItem);
        }
        private void VectorAdd_7_Collection(object instance, object item)
        {
            var collection = (global::System.Collections.Generic.ICollection<global::UWPContactApp.Contact>)instance;
            var newItem = (global::UWPContactApp.Contact)item;
            collection.Add(newItem);
        }

        private global::Windows.UI.Xaml.Markup.IXamlType CreateXamlType(int typeIndex)
        {
            global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlSystemBaseType xamlType = null;
            global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType userType;
            string typeName = _typeNameTable[typeIndex];
            global::System.Type type = _typeTable[typeIndex];

            switch (typeIndex)
            {

            case 0:   //  UWPContactApp.DateFormatConverter
                userType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType(this, typeName, type, GetXamlTypeByName("Object"));
                userType.Activator = Activate_0_DateFormatConverter;
                userType.SetIsLocalType();
                xamlType = userType;
                break;

            case 1:   //  Object
                xamlType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlSystemBaseType(typeName, type);
                break;

            case 2:   //  UWPContactApp.IntegerToStringConverter
                userType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType(this, typeName, type, GetXamlTypeByName("Object"));
                userType.Activator = Activate_2_IntegerToStringConverter;
                userType.SetIsLocalType();
                xamlType = userType;
                break;

            case 3:   //  UWPContactApp.MainPage
                userType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType(this, typeName, type, GetXamlTypeByName("Windows.UI.Xaml.Controls.Page"));
                userType.Activator = Activate_3_MainPage;
                userType.AddMemberName("Contacts");
                userType.SetIsLocalType();
                xamlType = userType;
                break;

            case 4:   //  Windows.UI.Xaml.Controls.Page
                xamlType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlSystemBaseType(typeName, type);
                break;

            case 5:   //  Windows.UI.Xaml.Controls.UserControl
                xamlType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlSystemBaseType(typeName, type);
                break;

            case 6:   //  System.Collections.ObjectModel.ObservableCollection`1<UWPContactApp.Contact>
                userType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType(this, typeName, type, GetXamlTypeByName("System.Collections.ObjectModel.Collection`1<UWPContactApp.Contact>"));
                userType.CollectionAdd = VectorAdd_6_ObservableCollection;
                userType.SetIsReturnTypeStub();
                xamlType = userType;
                break;

            case 7:   //  System.Collections.ObjectModel.Collection`1<UWPContactApp.Contact>
                userType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType(this, typeName, type, GetXamlTypeByName("Object"));
                userType.Activator = Activate_7_Collection;
                userType.CollectionAdd = VectorAdd_7_Collection;
                xamlType = userType;
                break;

            case 8:   //  UWPContactApp.Contact
                userType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType(this, typeName, type, GetXamlTypeByName("Object"));
                userType.Activator = Activate_8_Contact;
                userType.AddMemberName("Id");
                userType.AddMemberName("FirstName");
                userType.AddMemberName("LastName");
                userType.AddMemberName("DateOfBirth");
                userType.AddMemberName("StreetNumber");
                userType.AddMemberName("StreetName");
                userType.AddMemberName("PostCode");
                userType.AddMemberName("City");
                userType.AddMemberName("Phone1");
                userType.AddMemberName("Phone2");
                userType.AddMemberName("Email");
                userType.SetIsLocalType();
                xamlType = userType;
                break;

            case 9:   //  Int32
                xamlType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlSystemBaseType(typeName, type);
                break;

            case 10:   //  String
                xamlType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlSystemBaseType(typeName, type);
                break;

            case 11:   //  System.DateTime
                userType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType(this, typeName, type, GetXamlTypeByName("System.ValueType"));
                userType.SetIsReturnTypeStub();
                xamlType = userType;
                break;

            case 12:   //  System.ValueType
                userType = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType(this, typeName, type, GetXamlTypeByName("Object"));
                xamlType = userType;
                break;
            }
            return xamlType;
        }


        private object get_0_MainPage_Contacts(object instance)
        {
            var that = (global::UWPContactApp.MainPage)instance;
            return that.Contacts;
        }
        private void set_0_MainPage_Contacts(object instance, object Value)
        {
            var that = (global::UWPContactApp.MainPage)instance;
            that.Contacts = (global::System.Collections.ObjectModel.ObservableCollection<global::UWPContactApp.Contact>)Value;
        }
        private object get_1_Contact_Id(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.Id;
        }
        private void set_1_Contact_Id(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.Id = (global::System.Int32)Value;
        }
        private object get_2_Contact_FirstName(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.FirstName;
        }
        private void set_2_Contact_FirstName(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.FirstName = (global::System.String)Value;
        }
        private object get_3_Contact_LastName(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.LastName;
        }
        private void set_3_Contact_LastName(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.LastName = (global::System.String)Value;
        }
        private object get_4_Contact_DateOfBirth(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.DateOfBirth;
        }
        private void set_4_Contact_DateOfBirth(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.DateOfBirth = (global::System.DateTime)Value;
        }
        private object get_5_Contact_StreetNumber(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.StreetNumber;
        }
        private void set_5_Contact_StreetNumber(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.StreetNumber = (global::System.Int32)Value;
        }
        private object get_6_Contact_StreetName(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.StreetName;
        }
        private void set_6_Contact_StreetName(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.StreetName = (global::System.String)Value;
        }
        private object get_7_Contact_PostCode(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.PostCode;
        }
        private void set_7_Contact_PostCode(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.PostCode = (global::System.String)Value;
        }
        private object get_8_Contact_City(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.City;
        }
        private void set_8_Contact_City(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.City = (global::System.String)Value;
        }
        private object get_9_Contact_Phone1(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.Phone1;
        }
        private void set_9_Contact_Phone1(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.Phone1 = (global::System.String)Value;
        }
        private object get_10_Contact_Phone2(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.Phone2;
        }
        private void set_10_Contact_Phone2(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.Phone2 = (global::System.String)Value;
        }
        private object get_11_Contact_Email(object instance)
        {
            var that = (global::UWPContactApp.Contact)instance;
            return that.Email;
        }
        private void set_11_Contact_Email(object instance, object Value)
        {
            var that = (global::UWPContactApp.Contact)instance;
            that.Email = (global::System.String)Value;
        }

        private global::Windows.UI.Xaml.Markup.IXamlMember CreateXamlMember(string longMemberName)
        {
            global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember xamlMember = null;
            global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType userType;

            switch (longMemberName)
            {
            case "UWPContactApp.MainPage.Contacts":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.MainPage");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "Contacts", "System.Collections.ObjectModel.ObservableCollection`1<UWPContactApp.Contact>");
                xamlMember.Getter = get_0_MainPage_Contacts;
                xamlMember.Setter = set_0_MainPage_Contacts;
                break;
            case "UWPContactApp.Contact.Id":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "Id", "Int32");
                xamlMember.Getter = get_1_Contact_Id;
                xamlMember.Setter = set_1_Contact_Id;
                break;
            case "UWPContactApp.Contact.FirstName":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "FirstName", "String");
                xamlMember.Getter = get_2_Contact_FirstName;
                xamlMember.Setter = set_2_Contact_FirstName;
                break;
            case "UWPContactApp.Contact.LastName":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "LastName", "String");
                xamlMember.Getter = get_3_Contact_LastName;
                xamlMember.Setter = set_3_Contact_LastName;
                break;
            case "UWPContactApp.Contact.DateOfBirth":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "DateOfBirth", "System.DateTime");
                xamlMember.Getter = get_4_Contact_DateOfBirth;
                xamlMember.Setter = set_4_Contact_DateOfBirth;
                break;
            case "UWPContactApp.Contact.StreetNumber":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "StreetNumber", "Int32");
                xamlMember.Getter = get_5_Contact_StreetNumber;
                xamlMember.Setter = set_5_Contact_StreetNumber;
                break;
            case "UWPContactApp.Contact.StreetName":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "StreetName", "String");
                xamlMember.Getter = get_6_Contact_StreetName;
                xamlMember.Setter = set_6_Contact_StreetName;
                break;
            case "UWPContactApp.Contact.PostCode":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "PostCode", "String");
                xamlMember.Getter = get_7_Contact_PostCode;
                xamlMember.Setter = set_7_Contact_PostCode;
                break;
            case "UWPContactApp.Contact.City":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "City", "String");
                xamlMember.Getter = get_8_Contact_City;
                xamlMember.Setter = set_8_Contact_City;
                break;
            case "UWPContactApp.Contact.Phone1":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "Phone1", "String");
                xamlMember.Getter = get_9_Contact_Phone1;
                xamlMember.Setter = set_9_Contact_Phone1;
                break;
            case "UWPContactApp.Contact.Phone2":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "Phone2", "String");
                xamlMember.Getter = get_10_Contact_Phone2;
                xamlMember.Setter = set_10_Contact_Phone2;
                break;
            case "UWPContactApp.Contact.Email":
                userType = (global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlUserType)GetXamlTypeByName("UWPContactApp.Contact");
                xamlMember = new global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlMember(this, "Email", "String");
                xamlMember.Getter = get_11_Contact_Email;
                xamlMember.Setter = set_11_Contact_Email;
                break;
            }
            return xamlMember;
        }
    }

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class XamlSystemBaseType : global::Windows.UI.Xaml.Markup.IXamlType
    {
        string _fullName;
        global::System.Type _underlyingType;

        public XamlSystemBaseType(string fullName, global::System.Type underlyingType)
        {
            _fullName = fullName;
            _underlyingType = underlyingType;
        }

        public string FullName { get { return _fullName; } }

        public global::System.Type UnderlyingType
        {
            get
            {
                return _underlyingType;
            }
        }

        virtual public global::Windows.UI.Xaml.Markup.IXamlType BaseType { get { throw new global::System.NotImplementedException(); } }
        virtual public global::Windows.UI.Xaml.Markup.IXamlMember ContentProperty { get { throw new global::System.NotImplementedException(); } }
        virtual public global::Windows.UI.Xaml.Markup.IXamlMember GetMember(string name) { throw new global::System.NotImplementedException(); }
        virtual public bool IsArray { get { throw new global::System.NotImplementedException(); } }
        virtual public bool IsCollection { get { throw new global::System.NotImplementedException(); } }
        virtual public bool IsConstructible { get { throw new global::System.NotImplementedException(); } }
        virtual public bool IsDictionary { get { throw new global::System.NotImplementedException(); } }
        virtual public bool IsMarkupExtension { get { throw new global::System.NotImplementedException(); } }
        virtual public bool IsBindable { get { throw new global::System.NotImplementedException(); } }
        virtual public bool IsReturnTypeStub { get { throw new global::System.NotImplementedException(); } }
        virtual public bool IsLocalType { get { throw new global::System.NotImplementedException(); } }
        virtual public global::Windows.UI.Xaml.Markup.IXamlType ItemType { get { throw new global::System.NotImplementedException(); } }
        virtual public global::Windows.UI.Xaml.Markup.IXamlType KeyType { get { throw new global::System.NotImplementedException(); } }
        virtual public object ActivateInstance() { throw new global::System.NotImplementedException(); }
        virtual public void AddToMap(object instance, object key, object item)  { throw new global::System.NotImplementedException(); }
        virtual public void AddToVector(object instance, object item)  { throw new global::System.NotImplementedException(); }
        virtual public void RunInitializer()   { throw new global::System.NotImplementedException(); }
        virtual public object CreateFromString(string input)   { throw new global::System.NotImplementedException(); }
    }
    
    internal delegate object Activator();
    internal delegate void AddToCollection(object instance, object item);
    internal delegate void AddToDictionary(object instance, object key, object item);

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class XamlUserType : global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlSystemBaseType
    {
        global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlTypeInfoProvider _provider;
        global::Windows.UI.Xaml.Markup.IXamlType _baseType;
        bool _isArray;
        bool _isMarkupExtension;
        bool _isBindable;
        bool _isReturnTypeStub;
        bool _isLocalType;

        string _contentPropertyName;
        string _itemTypeName;
        string _keyTypeName;
        global::System.Collections.Generic.Dictionary<string, string> _memberNames;
        global::System.Collections.Generic.Dictionary<string, object> _enumValues;

        public XamlUserType(global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlTypeInfoProvider provider, string fullName, global::System.Type fullType, global::Windows.UI.Xaml.Markup.IXamlType baseType)
            :base(fullName, fullType)
        {
            _provider = provider;
            _baseType = baseType;
        }

        // --- Interface methods ----

        override public global::Windows.UI.Xaml.Markup.IXamlType BaseType { get { return _baseType; } }
        override public bool IsArray { get { return _isArray; } }
        override public bool IsCollection { get { return (CollectionAdd != null); } }
        override public bool IsConstructible { get { return (Activator != null); } }
        override public bool IsDictionary { get { return (DictionaryAdd != null); } }
        override public bool IsMarkupExtension { get { return _isMarkupExtension; } }
        override public bool IsBindable { get { return _isBindable; } }
        override public bool IsReturnTypeStub { get { return _isReturnTypeStub; } }
        override public bool IsLocalType { get { return _isLocalType; } }

        override public global::Windows.UI.Xaml.Markup.IXamlMember ContentProperty
        {
            get { return _provider.GetMemberByLongName(_contentPropertyName); }
        }

        override public global::Windows.UI.Xaml.Markup.IXamlType ItemType
        {
            get { return _provider.GetXamlTypeByName(_itemTypeName); }
        }

        override public global::Windows.UI.Xaml.Markup.IXamlType KeyType
        {
            get { return _provider.GetXamlTypeByName(_keyTypeName); }
        }

        override public global::Windows.UI.Xaml.Markup.IXamlMember GetMember(string name)
        {
            if (_memberNames == null)
            {
                return null;
            }
            string longName;
            if (_memberNames.TryGetValue(name, out longName))
            {
                return _provider.GetMemberByLongName(longName);
            }
            return null;
        }

        override public object ActivateInstance()
        {
            return Activator(); 
        }

        override public void AddToMap(object instance, object key, object item) 
        {
            DictionaryAdd(instance, key, item);
        }

        override public void AddToVector(object instance, object item)
        {
            CollectionAdd(instance, item);
        }

        override public void RunInitializer() 
        {
            System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(UnderlyingType.TypeHandle);
        }

        override public object CreateFromString(string input)
        {
            if (_enumValues != null)
            {
                int value = 0;

                string[] valueParts = input.Split(',');

                foreach (string valuePart in valueParts) 
                {
                    object partValue;
                    int enumFieldValue = 0;
                    try
                    {
                        if (_enumValues.TryGetValue(valuePart.Trim(), out partValue))
                        {
                            enumFieldValue = global::System.Convert.ToInt32(partValue);
                        }
                        else
                        {
                            try
                            {
                                enumFieldValue = global::System.Convert.ToInt32(valuePart.Trim());
                            }
                            catch( global::System.FormatException )
                            {
                                foreach( string key in _enumValues.Keys )
                                {
                                    if( string.Compare(valuePart.Trim(), key, global::System.StringComparison.OrdinalIgnoreCase) == 0 )
                                    {
                                        if( _enumValues.TryGetValue(key.Trim(), out partValue) )
                                        {
                                            enumFieldValue = global::System.Convert.ToInt32(partValue);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        value |= enumFieldValue; 
                    }
                    catch( global::System.FormatException )
                    {
                        throw new global::System.ArgumentException(input, FullName);
                    }
                }

                return value; 
            }
            throw new global::System.ArgumentException(input, FullName);
        }

        // --- End of Interface methods

        public Activator Activator { get; set; }
        public AddToCollection CollectionAdd { get; set; }
        public AddToDictionary DictionaryAdd { get; set; }

        public void SetContentPropertyName(string contentPropertyName)
        {
            _contentPropertyName = contentPropertyName;
        }

        public void SetIsArray()
        {
            _isArray = true; 
        }

        public void SetIsMarkupExtension()
        {
            _isMarkupExtension = true;
        }

        public void SetIsBindable()
        {
            _isBindable = true;
        }

        public void SetIsReturnTypeStub()
        {
            _isReturnTypeStub = true;
        }

        public void SetIsLocalType()
        {
            _isLocalType = true;
        }

        public void SetItemTypeName(string itemTypeName)
        {
            _itemTypeName = itemTypeName;
        }

        public void SetKeyTypeName(string keyTypeName)
        {
            _keyTypeName = keyTypeName;
        }

        public void AddMemberName(string shortName)
        {
            if(_memberNames == null)
            {
                _memberNames =  new global::System.Collections.Generic.Dictionary<string,string>();
            }
            _memberNames.Add(shortName, FullName + "." + shortName);
        }

        public void AddEnumValue(string name, object value)
        {
            if (_enumValues == null)
            {
                _enumValues = new global::System.Collections.Generic.Dictionary<string, object>();
            }
            _enumValues.Add(name, value);
        }
    }

    internal delegate object Getter(object instance);
    internal delegate void Setter(object instance, object value);

    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class XamlMember : global::Windows.UI.Xaml.Markup.IXamlMember
    {
        global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlTypeInfoProvider _provider;
        string _name;
        bool _isAttachable;
        bool _isDependencyProperty;
        bool _isReadOnly;

        string _typeName;
        string _targetTypeName;

        public XamlMember(global::UWPContactApp.UWPContactApp_XamlTypeInfo.XamlTypeInfoProvider provider, string name, string typeName)
        {
            _name = name;
            _typeName = typeName;
            _provider = provider;
        }

        public string Name { get { return _name; } }

        public global::Windows.UI.Xaml.Markup.IXamlType Type
        {
            get { return _provider.GetXamlTypeByName(_typeName); }
        }

        public void SetTargetTypeName(string targetTypeName)
        {
            _targetTypeName = targetTypeName;
        }
        public global::Windows.UI.Xaml.Markup.IXamlType TargetType
        {
            get { return _provider.GetXamlTypeByName(_targetTypeName); }
        }

        public void SetIsAttachable() { _isAttachable = true; }
        public bool IsAttachable { get { return _isAttachable; } }

        public void SetIsDependencyProperty() { _isDependencyProperty = true; }
        public bool IsDependencyProperty { get { return _isDependencyProperty; } }

        public void SetIsReadOnly() { _isReadOnly = true; }
        public bool IsReadOnly { get { return _isReadOnly; } }

        public Getter Getter { get; set; }
        public object GetValue(object instance)
        {
            if (Getter != null)
                return Getter(instance);
            else
                throw new global::System.InvalidOperationException("GetValue");
        }

        public Setter Setter { get; set; }
        public void SetValue(object instance, object value)
        {
            if (Setter != null)
                Setter(instance, value);
            else
                throw new global::System.InvalidOperationException("SetValue");
        }
    }
}

