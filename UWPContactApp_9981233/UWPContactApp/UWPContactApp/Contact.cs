﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWPContactApp
{
    public class Contact
    {
        #region Properties
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public int StreetNumber { get; set; }
        public string StreetName { get; set; }
        public string PostCode { get; set; }
        public string City { get; set; }

        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Email { get; set; }
        #endregion

        #region Constructors
        public Contact()
        {
        }

        public Contact(Contact contact)
        {
            this.Id = contact.Id;

            this.FirstName = contact.FirstName;
            this.LastName = contact.LastName;
            this.DateOfBirth = contact.DateOfBirth;

            this.StreetNumber = contact.StreetNumber;
            this.StreetName = contact.StreetName;
            this.PostCode = contact.PostCode;
            this.City = contact.City;

            this.Phone1 = contact.Phone1;
            this.Phone2 = contact.Phone2;
            this.Email = contact.Email;
        }

        public Contact(int id, string firstName, string lastName, DateTime dateOfBirth, int streetNumber, string streetName, string postCode, string city, string phone1, string phone2, string email)
        {
            Id = id;

            FirstName = firstName;
            LastName = lastName;
            DateOfBirth = dateOfBirth;

            StreetNumber = streetNumber;
            StreetName = streetName;
            PostCode = postCode;
            City = city;

            Phone1 = phone1;
            Phone2 = phone2;
            Email = email;
        }
        #endregion
    }
}
