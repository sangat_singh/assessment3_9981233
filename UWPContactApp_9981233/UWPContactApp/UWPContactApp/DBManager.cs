﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UWPContactApp
{
    class DBManager
    {
        private static string host = "localhost";
        private static string user = "root";
        private static string pass = "root";
        private static string dbname = "gui_comp6001_16b_assn3";

        private static MySqlConnection Connection(string h, string u, string p, string n)
        {
            string serverConnection = String.Format($"Server={h};Database={n};Uid={u};Pwd={p};SslMode=None;charset=utf8");
            MySqlConnection connection = new MySqlConnection(serverConnection);

            return connection;
        }

        public static MySqlConnection conn()
        {
            var a = Connection(host, user, pass, dbname);
            return a;
        }

        public class Results
        {
            public string Row { get; set; }
        }

        public static List<Results> SelectCommand(string command, MySqlConnection connection)
        {
            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            listTables.Add(new Results() { Row = $"{reader.GetValue(i)}" });
                        }
                    }
                }
            }

            return listTables;
        }

        public static ObservableCollection<Contact> ShowInList(string command)
        {
            ObservableCollection<Contact> dataList = new ObservableCollection<Contact>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i=i+11)
            {
                Contact contact = new Contact(int.Parse($"{list[i].Row}"), $"{list[i + 1].Row}", $"{list[i + 2].Row}", DateTime.Parse($"{list[i+3].Row}"), int.Parse($"{list[i+4].Row}"), $"{list[i+5].Row}", $"{list[i+6].Row}", $"{list[i+7].Row}", $"{list[i+8].Row}", $"{list[i+9].Row}", $"{list[i+10].Row}");
                dataList.Add(contact);
            }

            return dataList;
        }

        private static int GetLastId(MySqlConnection connection)
        {
            int lastId = -1;

            var command = $"SELECT max(id)FROM tbl_people";

            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        try
                        {
                            lastId = int.Parse($"{reader.GetValue(reader.FieldCount - 1)}");
                        }
                        catch
                        {
                            lastId = -1;
                        }
                    }
                }
            }

            return lastId;
        }

        // INSERT
        public static void AddContact(Contact contact)
        {
            MySqlConnection con = conn();
            int lastId = GetLastId(con);
            con.Open();
            
            MySqlCommand insertCommand = con.CreateCommand();

            insertCommand.CommandText = "INSERT INTO tbl_people(ID, FNAME, LNAME, DOB, Str_NUMBER, Str_NAME, POSTCODE, CITY, PHONE1, PHONE2, EMAIL)VALUES(@id, @firstName, @lastName, @dateOfBirth, @streetNumber, @streetName, @postcode, @city, @phone1, @phone2, @email)";
            insertCommand.Parameters.AddWithValue("@id", lastId+1);
            insertCommand.Parameters.AddWithValue("@firstName", contact.FirstName);
            insertCommand.Parameters.AddWithValue("@lastName", contact.LastName);
            insertCommand.Parameters.AddWithValue("@dateOfBirth", contact.DateOfBirth);
            insertCommand.Parameters.AddWithValue("@streetNumber", contact.StreetNumber);
            insertCommand.Parameters.AddWithValue("@streetName", contact.StreetName);
            insertCommand.Parameters.AddWithValue("@postcode", contact.PostCode);
            insertCommand.Parameters.AddWithValue("@city", contact.City);
            insertCommand.Parameters.AddWithValue("@phone1", contact.Phone1);
            insertCommand.Parameters.AddWithValue("@phone2", contact.Phone2);
            insertCommand.Parameters.AddWithValue("@email", contact.Email);
            insertCommand.ExecuteNonQuery();

            con.Clone();
        }

        // UPDATE
        public static void UpdateContact(Contact contact)
        {
            MySqlConnection con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            updateCommand.CommandText = "UPDATE tbl_people SET FNAME = @firstName, LNAME = @lastName, DOB = @dateOfBirth, Str_NUMBER = @streetNumber, Str_NAME = @streetName, POSTCODE = @postcode, CITY = @city, PHONE1 = @phone1, PHONE2 = @phone2, EMAIL = @email WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", contact.Id);
            updateCommand.Parameters.AddWithValue("@firstName", contact.FirstName);
            updateCommand.Parameters.AddWithValue("@lastName", contact.LastName);
            updateCommand.Parameters.AddWithValue("@dateOfBirth", contact.DateOfBirth);
            updateCommand.Parameters.AddWithValue("@streetNumber", contact.StreetNumber);
            updateCommand.Parameters.AddWithValue("@streetName", contact.StreetName);
            updateCommand.Parameters.AddWithValue("@postcode", contact.PostCode);
            updateCommand.Parameters.AddWithValue("@city", contact.City);
            updateCommand.Parameters.AddWithValue("@phone1", contact.Phone1);
            updateCommand.Parameters.AddWithValue("@phone2", contact.Phone2);
            updateCommand.Parameters.AddWithValue("@email", contact.Email);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

        // DELETE
        public static void DeleteContact(int id)
        {
            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            updateCommand.CommandText = "DELETE FROM tbl_people WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }
    }
}
