﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace UWPContactApp
{
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        #region Members and Properties
        public Contact CurrentContact;

        private ObservableCollection<Contact> contacts;
        public ObservableCollection<Contact> Contacts
        {
            get { return contacts; }
            set
            {
                if (value != contacts)
                {
                    contacts = value;
                    // Notify of the change.
                    NotifyPropertyChanged();
                }
            }
        }
        #endregion

        public MainPage()
        {
            this.InitializeComponent();
            // We set the preffered min window size
            ApplicationView.GetForCurrentView().SetPreferredMinSize(new Size(440, 100));
            FillForm();
            IconsListBox.SelectedIndex = 0;
            CurrentContact = new Contact();
            Contacts = new ObservableCollection<Contact>();
            Contacts = LoadAllTheData();
        }

        #region Callbacks
        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }

        private void IconsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem selectedItem = (ListBoxItem)IconsListBox.SelectedItem;

            switch (selectedItem.Name)
            {
                case "AddListBoxItem":
                    ExecuteButton.Content = "Add";
                    ExecuteButton.Background = new SolidColorBrush(Color.FromArgb(255, 179, 255, 170));
                        break;
                case "DeleteListBoxItem":
                    ExecuteButton.Content = "Delete";
                    ExecuteButton.Background = new SolidColorBrush(Color.FromArgb(255, 255, 170, 170));
                    break;
                case "UpdateListBoxItem":
                    ExecuteButton.Content = "Update";
                    ExecuteButton.Background = new SolidColorBrush(Color.FromArgb(255, 221, 170, 255));
                    break;
                default:
                    break;
            }
        }

        private void ExecuteButton_Click(object sender, RoutedEventArgs e)
        {
            switch ((string)ExecuteButton.Content)
            {
                case "Add":
                    Contact addContact = new Contact(-1, FirstNameTextBox.Text, LastNameTextBox.Text, DateTime.Parse(DateOfBirthTextBox.Text), int.Parse(StreetNumberTextBox.Text), StreetNameTextBox.Text, PostCodeTextBox.Text, CityTextBox.Text, Phone1TextBox.Text, Phone2TextBox.Text, EmailTextBox.Text);
                    DBManager.AddContact(addContact);
                    Contacts = LoadAllTheData();
                    ShowMessageBox("Contact added successfully");
                    break;
                case "Delete":
                    DBManager.DeleteContact(CurrentContact.Id);
                    Contacts = LoadAllTheData();
                    ShowMessageBox("Contact deleted successfully");
                    break;
                case "Update":
                    Contact updateContact = new Contact(CurrentContact.Id, FirstNameTextBox.Text, LastNameTextBox.Text, DateTime.Parse(DateOfBirthTextBox.Text), int.Parse(StreetNumberTextBox.Text), StreetNameTextBox.Text, PostCodeTextBox.Text, CityTextBox.Text, Phone1TextBox.Text, Phone2TextBox.Text, EmailTextBox.Text);
                    DBManager.UpdateContact(updateContact);
                    Contacts = LoadAllTheData();
                    ShowMessageBox("Contact updated successfully");
                    break;
                default:
                    break;
            }
        }

        private void ContactsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((Contact)ContactsList.SelectedItem == null)
                return;

            CurrentContact = new Contact((Contact)ContactsList.SelectedItem);

            FirstNameTextBox.Text = CurrentContact.FirstName;
            LastNameTextBox.Text = CurrentContact.LastName;
            DateOfBirthTextBox.Text = CurrentContact.DateOfBirth.ToString();

            StreetNumberTextBox.Text = CurrentContact.StreetNumber.ToString();
            StreetNameTextBox.Text = CurrentContact.StreetName;
            PostCodeTextBox.Text = CurrentContact.PostCode;
            CityTextBox.Text = CurrentContact.City;

            Phone1TextBox.Text = CurrentContact.Phone1;
            Phone2TextBox.Text = CurrentContact.Phone2;
            EmailTextBox.Text = CurrentContact.Email;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Reads the data from the database
        /// </summary>
        /// <returns></returns>
        static ObservableCollection<Contact> LoadAllTheData()
        {
            //Set the command and executes it and returns a list
            var command = $"Select * from tbl_people";

            //Call the custom method
            ObservableCollection<Contact> list = DBManager.ShowInList(command);

            //Display the list
            return list;
        }

        /// <summary>
        /// Fills the form with data
        /// </summary>
        void FillForm()
        {
            FirstNameTextBox.Text = "John";
            LastNameTextBox.Text = "Doe";
            DateOfBirthTextBox.Text = "7/8/1992 12:00:00 AM";

            StreetNumberTextBox.Text = "210";
            StreetNameTextBox.Text = "Sunset Boulevard";
            PostCodeTextBox.Text = "7500";
            CityTextBox.Text = "Los Angeles";

            Phone1TextBox.Text = "025354659";
            Phone2TextBox.Text = "045589325";
            EmailTextBox.Text = "johndoe@gmail.com";
        }

        /// <summary>
        /// Shows a message box
        /// </summary>
        /// <param name="message"></param>
        static async void ShowMessageBox(string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = "Database information:";

            dialog.Commands.Add(new UICommand { Label = "Ok", Id = 0 });
            dialog.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var res = await dialog.ShowAsync();

            if ((int)res.Id == 0)
            { }
        }
        #endregion

        #region INotifyPropertyChanged stuff
        /// <summary>
        /// PropertyChanged event
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// PropertyChanged event triggering method
        /// </summary>
        /// <param name="propertyName"></param>
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }


}
